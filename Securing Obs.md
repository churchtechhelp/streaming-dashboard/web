# Securing OBS WebSocket

In order for you to use the client on GitLab, you will need to secure the OBS websocket.

Generate a certificate. You will need to trust this on all your devices.
```
openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -days 365 --nodes
```

Configure NGINX
```
http {
  server {
    listen 4443 ssl default;
    server_name _;

    ssl_certificate     cert.pem;
    ssl_certificate_key key.pem;
    ssl_protocols       TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers         HIGH:!aNULL:!MD5;

    location / {
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header Host $host;

      proxy_pass https://ws-backend;

      proxy_ssl_certificate     /etc/ssl/certs/cert.pem;
      proxy_ssl_certificate_key /etc/ssl/private/key.pem;

      proxy_http_version 1.1;
      proxy_set_header Upgrade $http_upgrade;
      proxy_set_header Connection "upgrade";
    }
  }

  upstream ws-backend {
    # enable sticky session based on IP
    ip_hash;

    server localhost:4444;
  }
}
```